#!/bin/bash

#Dont accidentaly run this script on MacOS
if [[ "$OSTYPE" != "linux-gnu" ]]; then
echo "Cannot run this script on" $OSTYPE
exit 1
fi     

set -e

PROFTPD_CONF="asset/proftpd.conf"
PROFTPD_TLS="asset/tls.conf"
PROFTPD_CERT="certs/proftpd.cert.pem"
PROFTPD_KEY="certs/proftpd.key.pem"


if [ ! -f "$PROFTPD_CERT" ]; then
    echo "Did not find SSL cert in certs folder"
    dialog --title "SSL Certification" --msgbox 'Make sure SSL cert exists: certs/proftpd.cert.pem' 6 100
    exit 1
fi

if [ ! -f "$PROFTPD_KEY" ]; then
    dialog --title "SSL Certification" --msgbox 'Make sure SSL key exists: certs/proftpd.key.pem' 6 100
    exit 1
fi

echo "Please wait preparing installation"

apt-get  update -qq
apt-get  install -y -qq \
    dialog \
    > /dev/null

## CONFIGURATION SECTION #####################
FTP_URL="ftp.example.com"
DB_ROOT_PASSWORD="pass"
DB_PROFTPD_PASSWORD="pass"
SENDER_EMAIL="sender@mail.com"
SENDER_PASSWORD="pass"
RECIPIENT_EMAIL="recipient@mail.com"
SAMBA_USER="sambauser"
SAMBA_PASSWORD="pass"

## CONFIGURABLE SECTION #####################

fundialog=${fundialog=dialog}

# show an inputbox
FTP_URL=`$fundialog --stdout --title "Please Specify Script Settings" \
--backtitle "proftpd-mysql-administrtor build script" \
--inputbox "Specify the FTP URL:" 0 0 $FTP_URL` 

# show an inputbox
DB_ROOT_PASSWORD=`$fundialog --stdout --title "Please Specify Script Settings" \
--backtitle "proftpd-mysql-administrtor build script" \
--inputbox "MYSQL Root password:" 0 0 $DB_ROOT_PASSWORD` 

DB_PROFTPD_PASSWORD=`$fundialog --stdout --title "Please Specify Script Settings" \
--backtitle "proftpd-mysql-administrtor build script" \
--inputbox "MYSQL Proftpd password:" 0 0 $DB_PROFTPD_PASSWORD` 

SENDER_EMAIL=`$fundialog --stdout --title "Please Specify Script Settings" \
--backtitle "proftpd-mysql-administrtor build script" \
--inputbox "Email notification sender gmail:" 0 0 $SENDER_EMAIL` 

SENDER_PASSWORD=`$fundialog --stdout --title "Please Specify Script Settings" \
--backtitle "proftpd-mysql-administrtor build script" \
--inputbox "Email notification sender gmail password:" 0 0 $SENDER_PASSWORD` 

RECIPIENT_EMAIL=`$fundialog --stdout --title "Please Specify Script Settings" \
--backtitle "proftpd-mysql-administrtor build script" \
--inputbox "Mail notification recipient email:" 0 0 $RECIPIENT_EMAIL` 

SAMBA_USER=`$fundialog --stdout --title "Please Specify Script Settings" \
--backtitle "proftpd-mysql-administrtor build script" \
--inputbox "Samba user username (only lowercase):" 0 0 $SAMBA_USER` 

SAMBA_PASSWORD=`$fundialog --stdout --title "Please Specify Script Settings" \
--backtitle "proftpd-mysql-administrtor build script" \
--inputbox "Samba user password:" 0 0 $SAMBA_PASSWORD` 


clear


PROFTPD_INIT="asset/proftpd.init"
EMAILSCRIPT="asset/ftpmail"
EMAILAUTH="asset/ftpmail.auth"

debconf-set-selections <<< "postfix postfix/mailname string ftp.hostname.com"
debconf-set-selections <<< "postfix postfix/main_mailer_type string 'Internet Site'"
debconf-set-selections <<< 'mysql-server-5.6 mysql-server/root_password password '$DB_ROOT_PASSWORD''
debconf-set-selections <<< 'mysql-server-5.6 mysql-server/root_password_again password '$DB_ROOT_PASSWORD''
debconf-set-selections <<< 'proftpd-basic shared/proftpd/inetd_or_standalone select standalone'

echo "Updaing package list"
apt-get  -qq update
echo "Installing packages"
apt-get  install -y -qq \
    sudo \
    proftpd-mod-mysql \
    mysql-server \
    git \
    php5 php5-mysql \
    apache2 libapache2-mod-php5 \
    postfix \
    mailutils \
    libmail-sendmail-perl \
    samba \
     > /dev/null

echo "Configuring Server"
service proftpd stop
service apache2 stop
service smbd stop

PROFTPD_ADMIN_SOURCE="asset/proftpd_admin_v1.2.tar.gz"
PROFTPD_ADMIN_SOURCE_OUT="temp/admin_source"

#Extract the webui source
mkdir -p $PROFTPD_ADMIN_SOURCE_OUT
tar -xf  $PROFTPD_ADMIN_SOURCE -C $PROFTPD_ADMIN_SOURCE_OUT --strip-components 1 

#replace group and user for apache2
sed -i 's/APACHE_RUN_USER=www-data/APACHE_RUN_USER=nobody/g' /etc/apache2/envvars
sed -i 's/APACHE_RUN_GROUP=www-data/APACHE_RUN_GROUP=nogroup/g' /etc/apache2/envvars

#Add sudo abilityo for the nobody user to run the create_user script
echo 'nobody ALL=(ALL) NOPASSWD: /var/www/html/misc/user_script/create_user.sh' >> /etc/sudoers
echo 'nobody ALL=(ALL) NOPASSWD: /var/www/html/misc/user_script/delete_user.sh' >> /etc/sudoers

#Create the sql tables needed
sed -i 's/<database_password>/'$DB_PROFTPD_PASSWORD'/g' $PROFTPD_ADMIN_SOURCE_OUT/misc/database_structure_mysql/db_structure.sql
sed -i 's/TYPE=MyISAM/ENGINE=MyISAM/g' $PROFTPD_ADMIN_SOURCE_OUT/misc/database_structure_mysql/db_structure.sql
mysql --protocol tcp -h 127.0.0.1 --user=root --password=$DB_ROOT_PASSWORD < $PROFTPD_ADMIN_SOURCE_OUT/misc/database_structure_mysql/db_structure.sql

#Uncomment the load modules lines in modules.conf
sed -i 's/#LoadModule mod_sql.c/LoadModule mod_sql.c/g' /etc/proftpd/modules.conf
sed -i 's/#LoadModule mod_sql_mysql.c/LoadModule mod_sql_mysql.c/g' /etc/proftpd/modules.conf

#Copy the added configuration to conf.d
cp $PROFTPD_CONF /etc/proftpd/proftpd.conf
cp $PROFTPD_TLS /etc/proftpd/tls.conf

#copy in the SSL Certs
mkdir /etc/proftpd/ssl
cp certs/* /etc/proftpd/ssl/
chmod 600 /etc/proftpd/ssl/proftpd.*

#Update the root password
sed -i 's/<database_password>/'$DB_PROFTPD_PASSWORD'/g' /etc/proftpd/proftpd.conf

if [ -z "$FTP_URL" ]
then
      echo "Host not specified disabling dnsmasquarade"
else
      # #Update the domain url
    sed -i 's/<MYDOMAIN>/'$FTP_URL'/g' /etc/proftpd/proftpd.conf
    sed -i 's/#Masquer/Masquer/g' /etc/proftpd/proftpd.conf
fi


#remove apache default leftovers
rm /var/www/html/*

#copy web gui into web server dir
cp -rf $PROFTPD_ADMIN_SOURCE_OUT/* /var/www/html/
cp scripts/create_user.sh /var/www/html/misc/user_script/create_user.sh
chown nobody /var/www/html/misc/user_script/create_user.sh
chmod +x /var/www/html/misc/user_script/create_user.sh

cp scripts/delete_user.sh /var/www/html/misc/user_script/delete_user.sh
chown nobody /var/www/html/misc/user_script/delete_user.sh
chmod +x /var/www/html/misc/user_script/delete_user.sh

cp asset/configuration.xml /var/www/html/configuration.xml
chmod o+w /var/www/html/configuration.xml

#Automate configuration of the SQL database password
sed -i 's/<PROFTPD_MYSQL_PASSWORD>/'$DB_PROFTPD_PASSWORD'/g' /var/www/html/configuration.xml

#create ftp group. This wi
groupadd -g 10000 ftp_admin
groupadd -g 10001 ftp_user

#Create the ftp directories
mkdir -p /ftp/incoming
mkdir -p /ftp/users

chown nobody -R /ftp/users
chgrp ftp_admin -R /ftp/users

chown nobody -R /ftp/incoming
chgrp ftp_admin -R /ftp/incoming

chmod g+w /ftp/incoming
chmod g+w /ftp/users

#email setup
touch /etc/postfix/sasl_passwd
echo "[smtp.gmail.com]:587    $SENDER_EMAIL:$SENDER_PASSWORD" >> /etc/postfix/sasl_passwd
chmod 600 /etc/postfix/sasl_passwd

sed -i 's/relayhost =/relayhost = [smtp.gmail.com]:587/g' /etc/postfix/main.cf

echo "smtp_use_tls = yes"  >> /etc/postfix/main.cf
echo "smtp_sasl_auth_enable = yes"  >> /etc/postfix/main.cf
echo "smtp_sasl_security_options =" >> /etc/postfix/main.cf
echo "smtp_sasl_password_maps = hash:/etc/postfix/sasl_passwd" >> /etc/postfix/main.cf
echo "smtp_tls_CAfile = /etc/ssl/certs/ca-certificates.crt" >> /etc/postfix/main.cf

postmap /etc/postfix/sasl_passwd
systemctl restart postfix.service

#Copy the modified init script to start ftpmail and create the FIFO queue file.
cp $PROFTPD_INIT /etc/init.d/proftpd
cp $EMAILSCRIPT /usr/bin/ftpmail
cp $EMAILAUTH /etc/proftpd/ftpmail.auth

#Inject the emails and smtp server into the init script
sed -i 's/<MAILTO>/'$RECIPIENT_EMAIL'/g' /etc/init.d/proftpd
sed -i 's/<MAILFROM>/'$SENDER_EMAIL'/g' /etc/init.d/proftpd

systemctl daemon-reload

cp asset/smb.conf /etc/samba/smb.conf
adduser --disabled-password --gecos "" $SAMBA_USER
usermod -g ftp_user $SAMBA_USER
usermod -G ftp_admin $SAMBA_USER

(echo $SAMBA_PASSWORD; echo $SAMBA_PASSWORD) | smbpasswd -a $SAMBA_USER

echo "Starting Services"
#restart services
service proftpd start
service apache2 start
service smbd start

#Clean Up
rm -r temp


echo "Setup Complete, Enjoy!"

