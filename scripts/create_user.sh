#!/bin/bash
echo "Welcome to the create user script by Dimitrije Balanovic"
USER=$1
USER_ID=$2
GROUP_ID=$3
HOMEDIR=$4
EMAIL=$5

# echo "Creating user with data:"
# echo -e "\tUsername:\t" $USER
# echo -e "\tUser ID:\t" 	$USER_ID
# echo -e "\tGroup ID:\t" $GROUP_ID
# echo -e "\tHomedirectory:\t" $HOMEDIR
# echo -e "\tE-mail:\t\t" $EMAIL

echo "Setting up user: " $USER
echo ""

if [[ $HOMEDIR -ef /ftp/users ]]; then
    echo "Warning you have selected the ftp root as the user's home directory"
    echo $HOMEDIR
    echo "A user folder will not be created!"
    exit 1
fi


if [  -d $HOMEDIR ]; then
    echo "$HOMEDIR already exists!"
else
    echo "Creating $HOMEDIR"
    mkdir $HOMEDIR
fi

echo "Setting permissions for $HOMEDIR"
chown $USER_ID.$GROUP_ID $HOMEDIR
chmod 0774 $HOMEDIR

