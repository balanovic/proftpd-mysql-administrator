#!/bin/bash

sudo apt-get remove -y --purge proftpd-basic apache2 proftpd-mod-mysql mysql-server mysql-client mysql-common
sudo apt-get autoremove -y
sudo apt-get autoclean -y
sudo rm -r proftpd_admin
sudo rm -r /var/www
sudo rm -r /ftp