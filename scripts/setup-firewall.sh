#!/bin/bash

set -e

#SEtting up firewall
echo "Setting up firewall"

apt-get -qq update
apt-get -qq install -y \
    ufw \

ufw default deny incoming
ufw default allow outgoing
ufw allow ssh
ufw allow ftp

#passive ports
#ufw allow 1000:2000/tcp
ufw allow from 10.64.0.0/24
ufw allow from 10.64.30.0/24

ufw allow 60000:65535/tcp

ufw --force enable
ufw status verbose

