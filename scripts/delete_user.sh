#!/bin/bash
echo "Welcome to the delete user script by Dimitrije Balanovic"
USER=$1
USER_ID=$2
GROUP_ID=$3
HOMEDIR=$4
EMAIL=$5

# echo "Creating user with data:"
# echo -e "\tUsername:\t" $USER
# echo -e "\tUser ID:\t" 	$USER_ID
# echo -e "\tGroup ID:\t" $GROUP_ID
# echo -e "\tHomedirectory:\t" $HOMEDIR
# echo -e "\tE-mail:\t\t" $EMAIL

echo "Deleting user: " $USER
echo ""

if [[ $HOMEDIR -ef /ftp/users ]]; then
    echo "Warning user's homedir is the ftp root"
    echo $HOMEDIR
    echo "The folder will not be deleted!"
    exit 1
fi


if [  -d $HOMEDIR ]; then
    echo "$HOMEDIR exists!"
    echo "Data will not be removed. Please remote the user folder manually"
fi


