#!/bin/bash
set -e
echo "Please wait preparing installation"

apt-get  update -qq
apt-get  install -y -qq \
    dialog \
    > /dev/null

echo "Setting up VPN"

echo "Installing packages"
apt-get -qq update
apt-get -qq install -y \
    strongswan \
    > /dev/null


REMOTE_IP=""
EXTERNAL_IP=""
LOCALSUBNET=""
REMOTESUBNET=""
VPN_PSK=""

fundialog=${fundialog=dialog}

# show an inputbox
REMOTE_IP=`$fundialog --stdout --title "Please Specify Script Settings" \
--backtitle "VPN Setup Script" \
--inputbox "Specify the REMOTE IP address:" 0 0 $REMOTE_IP` 

# show an inputbox
EXTERNAL_IP=`$fundialog --stdout --title "Please Specify Script Settings" \
--backtitle "VPN Setup Script" \
--inputbox "Specify the External IP address of this machine:" 0 0 $EXTERNAL_IP` 

# show an inputbox
LOCALSUBNET=`$fundialog --stdout --title "Please Specify Script Settings" \
--backtitle "VPN Setup Script" \
--inputbox "Specify the local subnet e.g 10.64.5.0/24:" 0 0 $LOCALSUBNET` 

# show an inputbox
REMOTESUBNET=`$fundialog --stdout --title "Please Specify Script Settings" \
--backtitle "VPN Setup Script" \
--inputbox "Specify the remote subnet e.g 10.64.0.0/24:" 0 0 $REMOTESUBNET` 

# show an inputbox
VPN_PSK=`$fundialog --stdout --title "Please Specify Script Settings" \
--backtitle "proftpd-mysql-administrtor vpn script" \
--inputbox "Specify the VPN Pre Shared Key:" 0 0 $VPN_PSK` 


echo "Setting ipsec.conf"
cp asset/ipsec.conf /etc/ipsec.conf
sed -i 's/<localexternalip>/'$EXTERNAL_IP'/g' /etc/ipsec.conf
sed -i 's/<remoteip>/'$REMOTE_IP'/g' /etc/ipsec.conf
sed -i 's~<localsubnet>~'$LOCALSUBNET'~g' /etc/ipsec.conf
sed -i 's~<remotesubnet>~'$REMOTESUBNET'~g' /etc/ipsec.conf

echo "Setting ipsec.secret"
cp asset/ipsec.secrets /etc/ipsec.secrets
sed -i 's/local_ip/'$EXTERNAL_IP'/g' /etc/ipsec.secrets
sed -i 's/server_ip/'$REMOTE_IP'/g' /etc/ipsec.secrets
sed -i 's/vpn_psk/'$VPN_PSK'/g' /etc/ipsec.secrets

echo "Starting Services"
service ipsec restart
