# Proftpd Server Installation Script

This project contains an automated script to create a FTP server using proftpd and proftpd-administrator GUI 

## Requirements

- Clean install of debian jessie

## Instructions

if you dont have make and git install as follows
Note: run everything as super user "su"

Install git and make
- apt install make git

Clone the repo
- git clone git@gitlab.com:balanovic/proftpd-mysql-administrtor.git 

Run the make as root (not sudo) inside the project folder 

- make