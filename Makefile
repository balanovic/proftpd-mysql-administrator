PROJECT_NAME := "msgcent-api"


.PHONY: all dep build clean test coverage coverhtml lint

all: build


build: dep ## Build the binary file
	./scripts/build.sh;

clean: ## Remove previous build
	./scripts/clean.sh;

help: ## Display this help screen
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
